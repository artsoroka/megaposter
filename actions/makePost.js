var Promise = require('bluebird'); 
var client  = require(__dirname + '/../lib/client'); 

module.exports = function(config){
    return new Promise(function(resolve, reject){
//        client.on('error', function(e) {
//            reject(e.body.value.class, e.body.value.message);  
//        }); 
        
        client
            .init()
            .setViewportSize({
                width: 1024, 
                height: 2560
            }, false)
            .url('https://www.avito.ru')
            .deleteCookie('anid')
            .deleteCookie('auth')
            .deleteCookie('dfp_group')
            .deleteCookie('f')
            .deleteCookie('sessid')
            .deleteCookie('u')
            .deleteCookie('v')
            .loadCookies(config.cookies)
            .getCookie()
            .then(function(cookies){
                console.log('COOKIES: ', cookies); 
            })
            .url('https://www.avito.ru/profile')
            .saveScreenshot('./img/step1.png')
            .click('.btn-plain_blue')
            .pause(1000)
            .saveScreenshot('./img/step2.png')
            .click('div.form-category:nth-child(1) > label:nth-child(11) > span:nth-child(2)')
            .pause(500)
            .moveToObject('label.form-category-item:nth-child(51) > span:nth-child(2)')
            .click('label.form-category-item:nth-child(51) > span:nth-child(2)')
            .pause(500)
            .saveScreenshot('./img/step3.png')
            .pause(500)
            .click('#flt_param_820 > option:nth-child(5)')
            .scroll(0, 250)
            .then(function(){
                config.images.map(function(image){
                    client.chooseFile('input[name=image]', image); 
                    //client.pause(1000); 
                }); 
            })
            .setValue('#item-edit__title', config.post.title)
            .setValue('#item-edit__description', config.post.description)
            .setValue('#item-edit__price', config.post.price)
            .selectByValue('#flt_param_820', '11671')
            .pause(5000)
            .saveScreenshot('./img/step4.png')
            .click('li.packages__tab-nav:nth-child(3)')
            .pause(100)
            .click('div.packages__tab-panel__footer:nth-child(5) > div:nth-child(2) > button:nth-child(1)')
            .pause(100)
            .saveScreenshot('./img/step5.png')
            .end()
            .then(function(){
                resolve('done'); 
            }); 
    });
};
var client = require(__dirname + '/../lib/client'); 
var Promise = require('bluebird'); 

var Datastore   = require('nedb');
var cookieStore = new Datastore({ 
    filename: __dirname + '/../db/sessions.db', 
    autoload: true 
});

var login = function(config){
    var config = config || {}; 
    
    return new Promise(function(resolve, reject){
        var cookieObj = null;         
        client
            .init()
            .setViewportSize({
                width: 1280, 
                height: 1024
            }, false)
            .url('http://www.avito.ru/profile/login') 
            .saveScreenshot('./img/login.png')
            .setValue('input[name=login]', config.user.login)
            .setValue('input[name=password]', config.user.pass)
            .saveScreenshot('./img/login_filled.png')
            .click('.btn-yellow')
            .pause(1000)
            .saveScreenshot('./img/home.png')
            .getCookie().then(function(cookies) {
                cookieObj = cookies; 
            })
            .end()
            .then(function(){
                resolve({
                    cookies: cookieObj, 
                    post: config.post,
                    images: config.images
                }); 
            }); 
    
    });
}; 

module.exports = function(config){
    return new Promise(function(resolve, reject){
        cookieStore.find({ login: config.user.login }, function (err, docs) {
            if( err ) return console.log('db error'); 
            if( docs && docs.length ){
                console.log('USING EXISTING COOKIES');     
                return resolve({
                    cookies: docs[0].cookies, 
                    post: config.post,
                    images: config.images
                }); 
            
            } 
            console.log('SETTING UP COOKIES');   
            login(config).then(function(task){
                cookieStore.insert({login: config.user.login, cookies: task.cookies}, function(err, newDoc){
                    if( err ) return console.log('insert db error'); 
                    resolve({
                        cookies: newDoc.cookies, 
                        post: config.post,
                        images: config.images
                    });  
                }); 
            }); 
        });
    }); 
    
}; 

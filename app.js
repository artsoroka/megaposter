var client = require('./lib/client'); 
var login = require('./actions/getCookies'); 
var makePost = require('./actions/makePost'); 

var task = {
    user: {
        login: 'LOGIN', 
        pass: 'PASSWORD'
    },
  
    post: {
        title: 'some title',
        description: 'big chink of text here',
        price: 100500
    },
    
    images: [
        __dirname + '/images/kitten.jpg', 
        __dirname + '/images/puma.jpg',
        __dirname + '/images/kitten2.jpg', 
        __dirname + '/images/puma2.jpg'
    ]
    
};

login(task)
    .then(makePost)
    .then(function(msg){
        console.log(msg); 
    })
    .catch(function(err, msg){
        console.log(err, msg); 
    }); 

    
# Установка 

**Требования**

* Ununtu 14.04x64 
* Node.js >= 0.10 
* [Docker](http://docker.com)  
* [Chrome with Selenium pluging](https://github.com/peroumal1/docker-chrome-selenium) container

Для удобства можно использовать [PM2](https://github.com/Unitech/pm2)

## Установка Docker 

Для установки потребуется x64 ОС 

```
$ wget -qO- https://get.docker.com/ | sh
```

Чтобы не запускать ```Docker``` через ```sudo``` нужно добавить пользователя в группу ```docker```

```
$ user=$(whoami)
$ sudo usermod -a -G docker $user
``` 

В случае, если сервер подключается через прокси 

```
$ export http_proxy=http://PROXY:PORT
$ export https_proxy=http://PROXY:PORT
$ export socks_proxy=socks://PROXY:PORT
```
Для docker требуется укзать прокси в файле ```/etc/default/docker``` 

## Установка Node.js 

```
$ sudo apt-get update
$ curl -sL https://deb.nodesource.com/setup | sudo bash -
$ sudo apt-get install nodejs 
```

# Запуск проекта 

```
$ git clone clone https://bitbucket.org/artsoroka/megaposter.git
$ cd megaposter
$ npm install 
```

Запустить контейнер с Chrome 

```
$ docker run --privileged -p 4444:4444 -d eperoumalnaik/docker-chrome-selenium
```

Запуск через менеджер ```pm2``` 

```
pm2 start app.js 
```

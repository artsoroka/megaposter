var webdriverio = require('webdriverio'); 
var options = { desiredCapabilities: { browserName: 'chrome' } }; 
var client = webdriverio.remote(options);

client.addCommand('loadCookies', function(cookies){

    var self = this;

    return cookies.reduce(function (promiseChain, currentCookie) {
        return self.setCookie(currentCookie); 
    }, cookies);

}); 

module.exports = client; 